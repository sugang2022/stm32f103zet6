#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "lcd.h"
#include "usart.h"
#include "i2c.h"


uint8_t I2C_EE_Test(void);

int main(void)
{
	delay_init();	    	 //延时函数初始化	  
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置中断优先级分组为组2：2位抢占优先级，2位响应优先级
	uart_init(115200);	 	//串口初始化为115200
	LED_Init();		  		//初始化与LED连接的硬件接口
	I2C1_Init();

	printf("stm32 i2c master test:\r\n");

	I2C_EE_Test();

	while(1)
	{
		LED0=!LED0;//提示系统正在运行	
		delay_ms(300);	   
	}
}

uint8_t I2C_EE_Test(void)
{	
	uint8_t ReadData[256]={0};
	uint8_t WriteDdta[256]={0};
	uint16_t i;

	//初始化写入数组
	for(i = 0; i < 256; i++)
	{
		WriteDdta[i]=i; 
	}

	//向EEPROM从地址为0开始写入256个字节的数据 
	I2C_EE_BufferWrite(WriteDdta, 0, 256);
	//等待EEPROM写入数据完成 
	EEPROM_WaitForWriteEnd();	 
	//向EEPROM从地址为0开始读出256个字节的数据
	EEPROM_Read(ReadData, 0, 256);

	for (i=0; i<256; i++)
	{	
		if(ReadData[i] != WriteDdta[i])
		{
			EEPROM_ERROR("0x%02X ", ReadData[i]);
			EEPROM_ERROR("错误:I2C EEPROM写入与读出的数据不一致\n\r");
			return 0;
		}
		
		printf("0x%02X ", ReadData[i]);
		if(i%16 == 15)    
			printf("\n\r");   
	}
	
	EEPROM_INFO("I2C(AT24C02)读写测试成功\n\r");
	return 1;
}

void Delay(__IO uint32_t nCount)	 //简单的延时函数
{
	for(; nCount != 0; nCount--);
}



