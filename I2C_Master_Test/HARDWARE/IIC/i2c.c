#include "i2c.h"

//设置等待时间
static __IO uint32_t  I2CTimeout = I2CT_LONG_TIMEOUT;   

//等待超时，打印错误信息
static uint32_t I2C_TIMEOUT_UserCallback(uint8_t errorCode);

void I2C1_Init(void)
{
	GPIO_InitTypeDef    GPIO_InitStuctrue;
	I2C_InitTypeDef     I2C_InitStuctrue;

	//开启GPIO外设时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	//开启IIC外设时钟
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);

	//SCL引脚-复用开漏输出
	GPIO_InitStuctrue.GPIO_Mode = GPIO_Mode_AF_OD;
	GPIO_InitStuctrue.GPIO_Pin = GPIO_Pin_6;
	GPIO_InitStuctrue.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStuctrue);

	//SDA引脚-复用开漏输出
	GPIO_InitStuctrue.GPIO_Mode = GPIO_Mode_AF_OD;
	GPIO_InitStuctrue.GPIO_Pin = GPIO_Pin_7;
	GPIO_InitStuctrue.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStuctrue);

	//IIC结构体成员配置
	I2C_InitStuctrue.I2C_Ack = I2C_Ack_Enable;
	I2C_InitStuctrue.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2C_InitStuctrue.I2C_ClockSpeed = EEPROM_I2C_BAUDRATE;
	I2C_InitStuctrue.I2C_DutyCycle = I2C_DutyCycle_2;
	I2C_InitStuctrue.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStuctrue.I2C_OwnAddress1 = STM32_I2C_OWN_ADDR;
	I2C_Init(I2C1, &I2C_InitStuctrue);
	I2C_Cmd(I2C1, ENABLE);

}

//向EEPROM写入一个字节
void  EEPROM_Byte_Write(uint8_t addr,uint8_t data)
{
	//发送起始信号
	I2C_GenerateSTART(EEPROM_I2C,ENABLE);
	//检测EV5事件
	while( I2C_CheckEvent(EEPROM_I2C,I2C_EVENT_MASTER_MODE_SELECT)==ERROR);
	//发送设备写地址
	I2C_Send7bitAddress(EEPROM_I2C,EEPROM_I2C_Address,I2C_Direction_Transmitter);
	//检测EV6事件
	while( I2C_CheckEvent(EEPROM_I2C,I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)==ERROR);
	//发送要操作设备内部的地址
	I2C_SendData(EEPROM_I2C,addr);
	while( I2C_CheckEvent(EEPROM_I2C,I2C_EVENT_MASTER_BYTE_TRANSMITTING )==ERROR);
  	I2C_SendData(EEPROM_I2C,data);
	//检测EV8_2事件
	while( I2C_CheckEvent(EEPROM_I2C,I2C_EVENT_MASTER_BYTE_TRANSMITTED )==ERROR);
	//发送停止信号
	I2C_GenerateSTOP(EEPROM_I2C,ENABLE);
	
}

//向EEPROM写入多个字节
uint32_t  EEPROM_Page_Write(uint8_t addr,uint8_t *data,uint16_t Num_ByteToWrite)
{
	
	 I2CTimeout = I2CT_LONG_TIMEOUT;
	//判断IIC总线是否忙碌
	while(I2C_GetFlagStatus(EEPROM_I2C, I2C_FLAG_BUSY))   
	{
		if((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(1);
	} 
	//重新赋值
	I2CTimeout = I2CT_FLAG_TIMEOUT;
	//发送起始信号
	I2C_GenerateSTART(EEPROM_I2C,ENABLE);
	//检测EV5事件
	while( I2C_CheckEvent(EEPROM_I2C,I2C_EVENT_MASTER_MODE_SELECT)==ERROR)
	{
		 if((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(2);
	} 
	I2CTimeout = I2CT_FLAG_TIMEOUT;
	//发送设备写地址
	I2C_Send7bitAddress(EEPROM_I2C,EEPROM_I2C_Address,I2C_Direction_Transmitter);
	//检测EV6事件
	while( I2C_CheckEvent(EEPROM_I2C,I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)==ERROR)
	{
		 if((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(3);
	} 

	I2CTimeout = I2CT_FLAG_TIMEOUT;
	//发送要操作设备内部的地址
	I2C_SendData(EEPROM_I2C,addr);
	//检测EV8事件
	while( I2C_CheckEvent(EEPROM_I2C,I2C_EVENT_MASTER_BYTE_TRANSMITTING )==ERROR)
	{
		 if((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(4);
	} 

	while(Num_ByteToWrite)
	{
		I2C_SendData(EEPROM_I2C,*data);
		I2CTimeout = I2CT_FLAG_TIMEOUT;
		while( I2C_CheckEvent(EEPROM_I2C,I2C_EVENT_MASTER_BYTE_TRANSMITTING )==ERROR)
		{
				if((I2CTimeout--) == 0) return   I2C_TIMEOUT_UserCallback(5);
		} 
		 Num_ByteToWrite--;
		 data++;
	}

	I2CTimeout = I2CT_FLAG_TIMEOUT;
	//检测EV8_2事件
	while( I2C_CheckEvent(EEPROM_I2C,I2C_EVENT_MASTER_BYTE_TRANSMITTED )==ERROR)
	{
		if((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(6);
	} 
	//发送停止信号
	I2C_GenerateSTOP(EEPROM_I2C,ENABLE);
	 return 1;
}

//向EEPROM读取多个字节
uint32_t EEPROM_Read(uint8_t *data,uint8_t addr,uint16_t Num_ByteToRead)
{
	 I2CTimeout = I2CT_LONG_TIMEOUT;
	//判断IIC总线是否忙碌
	while(I2C_GetFlagStatus(EEPROM_I2C, I2C_FLAG_BUSY))   
	{
		if((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(1);
	} 
	
	I2CTimeout = I2CT_FLAG_TIMEOUT;
	//发送起始信号
	I2C_GenerateSTART(EEPROM_I2C,ENABLE);
	//检测EV5事件
	while( I2C_CheckEvent(EEPROM_I2C,I2C_EVENT_MASTER_MODE_SELECT )==ERROR)
	{
		if((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(7);
	} 
	
	I2CTimeout = I2CT_FLAG_TIMEOUT;
	//发送设备写地址
	I2C_Send7bitAddress(EEPROM_I2C,EEPROM_I2C_Address,I2C_Direction_Transmitter);
	//检测EV6事件等待从机应答
	while( I2C_CheckEvent(EEPROM_I2C,I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED )==ERROR)
	{
		if((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(8);
	}
  
	I2CTimeout = I2CT_FLAG_TIMEOUT;
	//发送要操作设备内部存储器的地址
	I2C_SendData(EEPROM_I2C,addr);
	//检测EV8事件
	while( I2C_CheckEvent(EEPROM_I2C,I2C_EVENT_MASTER_BYTE_TRANSMITTING )==ERROR)
	{
		if((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(9);
	}
	I2CTimeout = I2CT_FLAG_TIMEOUT;
	//发送起始信号
	I2C_GenerateSTART(EEPROM_I2C,ENABLE);
	//检测EV5事件
	while( I2C_CheckEvent(EEPROM_I2C,I2C_EVENT_MASTER_MODE_SELECT )==ERROR)
	{
		if((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(10);
	}
	I2CTimeout = I2CT_FLAG_TIMEOUT;	 
	//发送设备读地址
	I2C_Send7bitAddress(EEPROM_I2C,EEPROM_I2C_Address,I2C_Direction_Receiver);
	//检测EV6事件
	while( I2C_CheckEvent(EEPROM_I2C,I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED )==ERROR)
	{
		if((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(10);
	}
	 
	while(Num_ByteToRead--)
	{
		//是否是最后一个字节，若是则发送非应答信号
	if( Num_ByteToRead==0)
	{
		//发送非应答信号
		I2C_AcknowledgeConfig(EEPROM_I2C,DISABLE);
		//发送停止信号
		I2C_GenerateSTOP(EEPROM_I2C,ENABLE);
	}
	 
	I2CTimeout = I2CT_FLAG_TIMEOUT;	 
	//检测EV7事件
   	while( I2C_CheckEvent(EEPROM_I2C,I2C_EVENT_MASTER_BYTE_RECEIVED )==ERROR)
	{
		if((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(10);
	}
	 
	*data=I2C_ReceiveData(EEPROM_I2C);
	data++; 
	 
	}
	
	//重新开启应答信号
	I2C_AcknowledgeConfig(EEPROM_I2C,ENABLE);
  	return 1;
}
void I2C_EE_BufferWrite(uint8_t* pBuffer,uint8_t WriteAddr, uint16_t NumByteToWrite)
{
  u8 NumOfPage = 0, NumOfSingle = 0, Addr = 0, count = 0;
  //I2C_PageSize=8
  Addr = WriteAddr % I2C_PageSize;
  count = I2C_PageSize - Addr;
  NumOfPage =  NumByteToWrite / I2C_PageSize;
  NumOfSingle = NumByteToWrite % I2C_PageSize;
 
  /* 写入数据的地址对齐，对齐数为8 */
  if(Addr == 0) 
  {
    /* 如果写入的数据个数小于8 */
    if(NumOfPage == 0) 
    {
      EEPROM_Page_Write(WriteAddr, pBuffer, NumOfSingle);
      EEPROM_WaitForWriteEnd();
    }
    /* 如果写入的数据个数大于8 */
    else  
    {
			//按页写入
      while(NumOfPage--)
      {
        EEPROM_Page_Write(WriteAddr, pBuffer, I2C_PageSize); 
    	  EEPROM_WaitForWriteEnd();
        WriteAddr +=  I2C_PageSize;
        pBuffer += I2C_PageSize;
      }
      //不足一页(8个)单独写入
      if(NumOfSingle!=0)
      {
        EEPROM_Page_Write(WriteAddr, pBuffer, NumOfSingle);
        EEPROM_WaitForWriteEnd();
      }
    }
  }
  /*写的数据的地址不对齐*/
  else 
  {
      NumByteToWrite -= count;
      NumOfPage =  NumByteToWrite / I2C_PageSize;
      NumOfSingle = NumByteToWrite % I2C_PageSize;	
      
      if(count != 0)
      {  
        EEPROM_Page_Write(WriteAddr, pBuffer, count);
        EEPROM_WaitForWriteEnd();
        WriteAddr += count;
        pBuffer += count;
      } 
      
      while(NumOfPage--)
      {
        EEPROM_Page_Write(WriteAddr, pBuffer, I2C_PageSize);
        EEPROM_WaitForWriteEnd();
        WriteAddr +=  I2C_PageSize;
        pBuffer += I2C_PageSize;  
      }
      if(NumOfSingle != 0)
      {
        EEPROM_Page_Write(WriteAddr, pBuffer, NumOfSingle); 
        EEPROM_WaitForWriteEnd();
      }
    } 
}

uint32_t EEPROM_WaitForWriteEnd(void)
{
	I2CTimeout = I2CT_FLAG_TIMEOUT;	
	
	do
	{
		I2CTimeout = I2CT_FLAG_TIMEOUT;
		//发送起始信号
		I2C_GenerateSTART(EEPROM_I2C,ENABLE);
		//检测EV5事件
		while( I2C_GetFlagStatus(EEPROM_I2C,I2C_FLAG_SB )==RESET)
		{
					if((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(10);
			}
		I2CTimeout = I2CT_FLAG_TIMEOUT;	
		//发送设备写地址
		I2C_Send7bitAddress(EEPROM_I2C,EEPROM_I2C_Address,I2C_Direction_Transmitter);
		
	}while( (I2C_GetFlagStatus(EEPROM_I2C,I2C_FLAG_ADDR )==RESET) && (I2CTimeout--) );
	
	//发送停止信号
	I2C_GenerateSTOP(EEPROM_I2C,ENABLE);
	return 1;
}



static  uint32_t I2C_TIMEOUT_UserCallback(uint8_t errorCode)
{
  /* Block communication and all processes */
  EEPROM_ERROR("I2C 等待超时!errorCode = %d",errorCode);
  
  return 0;
}




























