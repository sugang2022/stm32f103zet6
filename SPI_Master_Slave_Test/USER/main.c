#include "string.h"
#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "usart.h"	 
#include "spi.h"


const u8 TEXT_Buffer[]={0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26};
u8 Rev_Buffer[256]={0x00};
u16 datacount = 0;
#define DATA_SIZE sizeof(TEXT_Buffer)

void SPI1_Write_Data(u8* pData, u32 datalen);

int main(void)
{
	u16 i; 
	delay_init();	    	 //延时函数初始化	  
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置中断优先级分组为组2：2位抢占优先级，2位响应优先级
	uart_init(115200);	 	//串口初始化为115200
	LED_Init();
	SPI1_Init();
	SPI2_Init();

	printf("stm32 spi master slave test:\r\n");

	while(1)
	{
		SPI1_Write_Data((u8*)TEXT_Buffer, DATA_SIZE);
		LED0=!LED0;
		if(datacount == 16)
		{
			SPI_Cmd(SPI2, DISABLE);
			for(i=0; i<datacount; i++)
			{ 
				printf("%02x ",Rev_Buffer[i]);
			}
			memset(Rev_Buffer, 0, 256);
			datacount = 0;
			SPI_Cmd(SPI2, ENABLE);
		}
		delay_ms(300);  
	}
}

void SPI1_Write_Data(u8* pData, u32 datalen)   
{ 		
 	u16 i;

	SPI1_NSS=0;//片选

	for(i = 0; i < datalen; i++)
	{
		SPI1_ReadWriteByte(pData[i]);//循环写数    
	}

	SPI1_NSS=1; //取消片选 	    
}

void SPI2_IRQHandler(void)
{
	/* 判断接收缓冲区是否为非空 */
	if (SET == SPI_I2S_GetITStatus(SPI2, SPI_I2S_IT_RXNE))
	{
		/* 清中断标志 */
		SPI_I2S_ClearITPendingBit(SPI2, SPI_I2S_IT_RXNE);
		
		Rev_Buffer[datacount] = SPI2->DR;/* 读取接收缓冲区数据 */
		datacount++;
	}
}
