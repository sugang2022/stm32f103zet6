#include "dac.h"


uint32_t DAC_Sin[128];
uint16_t CH_1[] = {3089,3089,3089,3089,3089,3089,3089,3089,
	3089,3089,3089,3089,3089,3089,3089,3089,
	3089,3089,3089,3089,3089,3089,3089,3089,
	3089,3089,3089,3089,3089,3089,3089,3089,
	3089,3089,3089,3089,3089,3089,3089,2873,
	2606,2556,2826,3267,3567,3475,2995,2409,
	2107,2316,2930,3557,3766,3382,2618,1955,
	1833,2352,3191,3800,3761,3066,2140,1578,
	1752,2565,3496,3929,3560,2600,1655,1334,
	1853,2885,3759,3894,3179,2055,1239,1258,
	2106,3237,3912,3676,2660,1506,950,1354,
	2458,3545,3902,3279,2060,1021,826,1601,
	2843,3742,3704,2735,1446,657,873,1950,
	3187,3773,3316,2094,881,450,1072,2336,
	3418,3605,2761,1414,419,405,1369,2677,
	3469,3217,2065,736,65,461,1619,2918,
	396,2794,1526,431,232,1075,2430,3440,
	3471,2518,1213,409,635,1755,3058,3716,
	3321,2143,956,533,1153,2423,3533,3781,
	3024,1766,829,821,1751,3022,3827,3661,
	2651,1461,867,1251,2366,3498,3929,3401,
	2273,1285,1076,1777,2934,3808,3853,3062,
	1964,1274,1434,2333,3390,3934,3637,2717,
	1781,1436,1894,2848,3684,3883,3343,2441,
	1769,1753,2386,3247,3789,3689,3049,2307,
	1946,2177,2826,3462,3693,3413,2849,2390,
	2320,2642,3104,3395,3366,3121,2926,3089,
	3089,3089,3089,3089,3089,3089,3089,3089,
	3089,3089,3089,3089,3089,3089,3089,3089,
	3089,3089,3089,3089,3089,3089,3089,3089,
    3089,3089,3089,3089,3089,3089,3089,3089,
    3089,3089,3089,3089,3089,3089,3089,3089
};

void Dac1_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	DAC_InitTypeDef DAC_InitType;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE );
   	RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE );

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
 	GPIO_Init(GPIOA, &GPIO_InitStructure);
	GPIO_SetBits(GPIOA,GPIO_Pin_4);
					
	DAC_InitType.DAC_Trigger=DAC_Trigger_T2_TRGO;
	DAC_InitType.DAC_WaveGeneration=DAC_WaveGeneration_Noise;
//	DAC_InitType.DAC_LFSRUnmask_TriangleAmplitude=DAC_LFSRUnmask_Bit0;
	DAC_InitType.DAC_OutputBuffer=DAC_OutputBuffer_Disable ;
    DAC_Init(DAC_Channel_1,&DAC_InitType);
	DAC_Cmd(DAC_Channel_1, ENABLE);
  
    DAC_SetChannel1Data(DAC_Align_12b_R, 0);

}

void DAC_TIM_Config(void)
{
	TIM_TimeBaseInitTypeDef TIM_TimetypeDef;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);
	TIM_TimetypeDef.TIM_Prescaler = (72-1);
	TIM_TimetypeDef.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimetypeDef.TIM_Period = (1000-1);
	TIM_TimetypeDef.TIM_ClockDivision = TIM_CKD_DIV1;	
	TIM_TimetypeDef.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM2,&TIM_TimetypeDef);
	
	TIM_SelectOutputTrigger(TIM2, TIM_TRGOSource_Update);
	TIM_Cmd(TIM2,ENABLE);
}

static void DAC_DMA_Config(void)
{
	DMA_InitTypeDef DMA_typeDef;
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA2,ENABLE);
	DMA_typeDef.DMA_PeripheralBaseAddr = 0x40007420;
	DMA_typeDef.DMA_MemoryBaseAddr = (uint32_t)DAC_Sin;
	DMA_typeDef.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_typeDef.DMA_BufferSize = 256;
	DMA_typeDef.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_typeDef.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_typeDef.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Word;
	DMA_typeDef.DMA_MemoryDataSize = DMA_MemoryDataSize_Word;
	DMA_typeDef.DMA_Mode = DMA_Mode_Circular;
	DMA_typeDef.DMA_Priority = DMA_Priority_High;
	DMA_typeDef.DMA_M2M = DMA_M2M_Disable;

	DMA_Init(DMA2_Channel3,&DMA_typeDef);
	DMA_Cmd(DMA2_Channel3, ENABLE);

	DAC_DMACmd(DAC_Channel_1, ENABLE);
}


void User_DAC_Init(void)
{
	uint16_t i;
	Dac1_Init();
	DAC_TIM_Config();
	DAC_DMA_Config();
	
	for(i=0; i<256; i++)
	{
		DAC_Sin[i] = CH_1[i];
	}
}


//设置通道1输出电压
//vol:0~3300,代表0~3.3V
void Dac1_Set_Vol(u16 vol)
{
	float temp=vol;
	temp/=1000;
	temp=temp*4096/3.3;
	DAC_SetChannel1Data(DAC_Align_12b_R,temp);//12位右对齐数据格式设置DAC值
}





















































