#ifndef __I2C_H
#define __I2C_H
#include <stdio.h>
#include "sys.h"

//IIC1
#define  EEPROM_I2C                       I2C1
#define  EEPROM_I2C_BAUDRATE              400000

//STM32自身地址1 与从机设备地址不相同即可(7位地址)
#define   STM32_I2C_OWN_ADDR             0x6f
//EEPROM设备地址
#define   EEPROM_I2C_Address             0XA0
#define   I2C_PageSize                     8

//等待次数
#define I2CT_FLAG_TIMEOUT         ((uint32_t)0x1000)
#define I2CT_LONG_TIMEOUT         ((uint32_t)(10 * I2CT_FLAG_TIMEOUT))

/*信息输出*/
#define EEPROM_DEBUG_ON                    0
#define EEPROM_INFO(fmt,arg...)           printf("<<-EEPROM-INFO->> "fmt"\n",##arg)
#define EEPROM_ERROR(fmt,arg...)          printf("<<-EEPROM-ERROR->> "fmt"\n",##arg)
#define EEPROM_DEBUG(fmt,arg...)          do{\
                                          if(EEPROM_DEBUG_ON)\
                                          printf("<<-EEPROM-DEBUG->> [%d]"fmt"\n",__LINE__, ##arg);\
                                          }while(0)

void I2C1_Init(void);
void EEPROM_Byte_Write(uint8_t addr,uint8_t data);	
uint32_t  EEPROM_WaitForWriteEnd(void);	
uint32_t  EEPROM_Page_Write(uint8_t addr,uint8_t *data,uint16_t Num_ByteToWrite);																					
uint32_t  EEPROM_Read(uint8_t *data,uint8_t addr,uint16_t Num_ByteToRead);
void I2C_EE_BufferWrite(uint8_t* pBuffer,uint8_t WriteAddr, uint16_t NumByteToWrite);

	 
#endif

