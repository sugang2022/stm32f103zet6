#include "string.h"
#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "lcd.h"
#include "usart.h"	 
#include "spi.h"
#include "w25qxx.h" 

u16 datacount = 0;
u8 Rev_Buffer[256]={0x00};

void SPI2_Read_Data(u8* pData, u32 datalen);

int main(void)
{
	u16 i; 
	delay_init();	    	 //延时函数初始化	  
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置中断优先级分组为组2：2位抢占优先级，2位响应优先级
	uart_init(115200);	 	//串口初始化为115200
	LED_Init();		  		//初始化与LED连接的硬件接口
	SPI2_Init();

	printf("stm32 spi slave test:\r\n");

	while(1)
	{
		if(datacount == 16)
		{
			LED0=!LED0;//提示系统正在运行	
			SPI_Cmd(SPI2, DISABLE);
			for(i=0; i<datacount; i++)
			{ 
				printf("%02x ",Rev_Buffer[i]);
			}
			memset(Rev_Buffer, 0, 256);
			datacount = 0;
			SPI_Cmd(SPI2, ENABLE);
		}
//		delay_ms(300);	   
	}
}


void SPI2_IRQHandler(void)
{
	/* 判断接收缓冲区是否为非空 */
	if (SET == SPI_I2S_GetITStatus(SPI2, SPI_I2S_IT_RXNE))
	{
		/* 清中断标志 */
		SPI_I2S_ClearITPendingBit(SPI2, SPI_I2S_IT_RXNE);
		
		Rev_Buffer[datacount] = SPI2->DR;/* 读取接收缓冲区数据 */
		datacount++;
	}

}
