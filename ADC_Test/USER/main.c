#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "lcd.h"
#include "usart.h"
#include "adc.h"
 
int main(void)
 {	 
	u16 adcx;
	float temp;
	delay_init();	    	 //延时函数初始化	  
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置中断优先级分组为组2：2位抢占优先级，2位响应优先级
	uart_init(115200);	 	//串口初始化为115200
 	LED_Init();			     //LED端口初始化
	LCD_Init();			 	
 	Adc_Init();		  		//ADC初始化
       
	while(1)
	{
		adcx=Get_Adc_Average(ADC_Channel_1,10);
		printf("adcx = %d\r\n", adcx);
		temp=(float)adcx*(3.3/4096);
		adcx=temp;
		printf("adcx = %d\r\n", adcx);
		temp-=adcx;
		temp*=1000;
		printf("temp = %f\r\n", temp);
		LED0=!LED0;
		delay_ms(250);	
	}
 }




