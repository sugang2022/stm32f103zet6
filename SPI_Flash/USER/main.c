#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "lcd.h"
#include "usart.h"	 
#include "spi.h"
#include "w25qxx.h"
#include "hw_config.h"
#include "usb_lib.h"
#include "usb_pwr.h"
#include "malloc.h"	 
#include "mass_mal.h"
#include "memory.h"	    
#include "usb_bot.h"
#include "ff.h"

const u8 TEXT_Buffer[]={"HELLO WORLD123456 STM32 SPI TEST"};
#define SIZE sizeof(TEXT_Buffer)

volatile static u8 gFsInited = 0;
u32 FLASH_SIZE_32M = 32*1024*1024;    //FALSH 大小为32M字节
u32 FLASH_SIZE_16M = 16*1024*1024;    //FALSH 大小为16M字节
const u8 buffer[] = {0xFA,0xFB,0x11,0x22,0xAB,0x55,0x91,0xFC};
u8 data[16];
FATFS fs;
FIL file;

void Set_Mass_Size(u16 wqxxtype);
void FileSystemInit(u16 typeid);

int main(void)
{
	u8 key;
	u16 i=0;
	u8 datatemp[SIZE];
	u32 FLASH_SIZE; 
    u16 id = 0;
	delay_init();
	uart_init(115200);
	LED_Init();
	KEY_Init();
	W25QXX_Init();
    FileSystemInit(W25QXX_TYPE);
	Set_Mass_Size(W25QXX_TYPE);
    USB_Config();
	printf("stm32 spi master test:\r\n");

	while(1)
	{
//		LED0=!LED0;
//		delay_ms(300);
		key=KEY_Scan(0);
		if(key==KEY1_PRES)	//KEY1按下,写入W25QXX
		{   
 			printf("Start Write W25Q128....\r\n"); 
			W25QXX_Write((u8*)TEXT_Buffer,FLASH_SIZE-100,SIZE);			//从倒数第100个地址处开始,写入SIZE长度的数据
			printf("W25Q128 Write Finished!\r\n");	//提示传送完成
		}
		if(key==KEY0_PRES)	//KEY0按下,读取字符串并显示
		{
 			printf("Start Read W25Q128.... \r\n");
			W25QXX_Read(datatemp,FLASH_SIZE-100,SIZE);					//从倒数第100个地址处开始,读出SIZE个字节
			printf("The Data Readed Is:  \r\n");
			printf("%s\r\n",datatemp);
		}
		i++;
		delay_ms(10);
		if(i==20)
		{
			LED0=!LED0;//提示系统正在运行	
			i=0;
		}	
	}
}

void Set_Mass_Size(u16 wqxxtype)
{
	
	 if(wqxxtype == W25Q128)
    {

        Mass_Memory_Size[0]=(1024*1024*15);
    }
    else if(wqxxtype == W25Q256)
    {

        Mass_Memory_Size[0]=(1024*1024*30);
    }
    Mass_Block_Size[0] =512;
    Mass_Block_Count[0]=Mass_Memory_Size[0]/Mass_Block_Size[0];

    Data_Buffer=mymalloc(BULK_MAX_PACKET_SIZE*2*4);
    Bulk_Data_Buff=mymalloc(BULK_MAX_PACKET_SIZE);
	
}

void FileSystemInit(u16 typeid)
{
    int res;
    FRESULT ores;
        
    if(typeid == W25Q128)
    {
        W25QXX_Read((u8 *)data, (FLASH_SIZE_16M - 1024), sizeof(buffer));

        res = memcmp(buffer, data, sizeof(buffer));

        if(res == 0)
        {    
            //W25QXX_Erase_Chip();
            delay_ms(10);
            W25QXX_Read((u8 *)data, (FLASH_SIZE_16M - 1024), sizeof(buffer));
            //Test_sendbuf((int8u *)&data, sizeof(buffer));
            ores = f_mount(&fs, (const TCHAR *)"0:" , 1);
        }
        else
        {
            //Test_sendbuf((int8u *)&data, sizeof(buffer));
            ores = f_mount(&fs, (const TCHAR *)"0:" , 1);

            if(ores == FR_NO_FILESYSTEM)
            {
                ores = f_mkfs((const TCHAR *)"0:", 0, 4096);
                
                if(ores == 0)
                {
                    res = f_mount(NULL, (const TCHAR *)"0:" , 1);
                    res = f_mount(&fs, (const TCHAR *)"0:" , 1);
                
                    if(res == 0)
                    {
                        gFsInited =1;
                    }
                }
            }
            
            W25QXX_Write((u8 *)buffer, (FLASH_SIZE_16M - 1024), sizeof(buffer));
        }

    }
    else if(typeid == W25Q256)
    {
        W25QXX_Read((u8 *)data, (FLASH_SIZE_32M - 1024), sizeof(buffer));
        res = memcmp(buffer, data, sizeof(buffer));
        
        if(res == 0)
        {
            //W25QXX_Erase_Chip();
            delay_ms(10);
            ores = f_mount(&fs, (const TCHAR *)"0:" , 1);
            //Test_sendbuf((int8u *)&data, sizeof(buffer));
        }
        else
        {
            //Test_sendbuf((int8u *)&data, sizeof(buffer));
            ores = f_mount(&fs, (const TCHAR *)"0:" , 1);

            if(ores == FR_NO_FILESYSTEM)
            {
                ores = f_mkfs((const TCHAR *)"0:", 0, 4096);
                
                if(ores == 0)
                {
                    res = f_mount(NULL, (const TCHAR *)"0:" , 1);
                    res = f_mount(&fs, (const TCHAR *)"0:" , 1);
                
                    if(res == 0)
                    {
                        gFsInited =1;
                
                    }
                }

            }

            W25QXX_Write((u8 *)buffer, (FLASH_SIZE_32M - 1024), sizeof(buffer));
        }
    }
}
