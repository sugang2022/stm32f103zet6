#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "usart.h"
#include "tm1637.h"

unsigned char i=0;
extern unsigned char SigNum[24];
	
int main(void)
{
	delay_init();	    	 //延时函数初始化	  
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置中断优先级分组为组2：2位抢占优先级，2位响应优先级
	uart_init(115200);	 	//串口初始化为115200
	LED_Init();		  		//初始化与LED连接的硬件接口
    TM1637_Init();

	while(1)
	{
		for(i = 0; i < 10; i++)
		{
			TM1637_DisplayValue(SigNum[i]);
			LED0=!LED0;//提示系统正在运行	
			delay_ms(1000);
		}	   
	}
}
