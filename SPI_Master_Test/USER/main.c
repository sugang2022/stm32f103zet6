#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "lcd.h"
#include "usart.h"	 
#include "spi.h"
#include "w25qxx.h" 

			 	
//要写入到W25Q64的字符串数组
// const u8 TEXT_Buffer[]={"STM32 SPI Data Test"};
const u8 TEXT_Buffer[]={0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26};
#define DATA_SIZE sizeof(TEXT_Buffer)

void SPI2_Write_Data(u8* pData, u32 datalen);

int main(void)
{
	delay_init();	    	 //延时函数初始化	  
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置中断优先级分组为组2：2位抢占优先级，2位响应优先级
	uart_init(115200);	 	//串口初始化为115200
	LED_Init();		  		//初始化与LED连接的硬件接口
	SPI2_Init();

	printf("stm32 spi master test:\r\n");

	while(1)
	{
		SPI2_Write_Data((u8*)TEXT_Buffer, DATA_SIZE);
		LED0=!LED0;//提示系统正在运行	
		delay_ms(300);	   
	}
}


//void SPI2_Write_Data(u8* pData, u32 datalen)   
//{ 		
// 	u16 i;

//	// SPI2_NSS=0;//片选

//	for(i = 0; i < datalen; i++)
//	{
//		SPI2_NSS=0;//片选
//		SPI2_ReadWriteByte(pData[i]);//循环写数  
//		SPI2_NSS=1; //取消片选 
//		delay_us(100);		  
//	}

//	// SPI2_NSS=1; //取消片选 	    
//} 

void SPI2_Write_Data(u8* pData, u32 datalen)   
{ 		
 	u16 i;

	SPI2_NSS=0;//片选

	for(i = 0; i < datalen; i++)
	{
		SPI2_ReadWriteByte(pData[i]);//循环写数    
	}

	SPI2_NSS=1; //取消片选 	    
}
