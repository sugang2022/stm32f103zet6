#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "lcd.h"
#include "usart.h"
#include "i2c.h"


extern uint8_t I2C_Buffer_Rx[128];
extern uint8_t I2C_Buffer_Tx[128];
extern uint32_t RecvLen;
extern uint8_t RecvFlag;

int main(void)
{
	uint32_t i=0;
	delay_init();	    	 //延时函数初始化	  
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置中断优先级分组为组2：2位抢占优先级，2位响应优先级
	uart_init(115200);	 	//串口初始化为115200
	LED_Init();		  		//初始化与LED连接的硬件接口
	I2C1_Init();

	printf("stm32 i2c slave test:\r\n");

	while(1)
	{
//		LED0=!LED0;//提示系统正在运行	
//		delay_ms(300);	   
	}
}


void Delay(__IO uint32_t nCount)	 //简单的延时函数
{
	for(; nCount != 0; nCount--);
}

