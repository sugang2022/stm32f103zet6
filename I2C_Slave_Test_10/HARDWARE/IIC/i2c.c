#include "i2c.h"

//设置等待时间
static __IO uint32_t  I2CTimeout = I2CT_LONG_TIMEOUT;   
void NVIC_Configuration(void);
void I2C1_Ram_Init(void) ;

void I2C1_Init(void)
{
	GPIO_InitTypeDef    GPIO_InitStuctrue;
	I2C_InitTypeDef     I2C_InitStuctrue;

	//开启GPIO外设时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	//开启IIC外设时钟
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);

	//SCL引脚-复用开漏输出
	GPIO_InitStuctrue.GPIO_Mode = GPIO_Mode_AF_OD;
	GPIO_InitStuctrue.GPIO_Pin = GPIO_Pin_6;
	GPIO_InitStuctrue.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStuctrue);

	//SDA引脚-复用开漏输出
	GPIO_InitStuctrue.GPIO_Mode = GPIO_Mode_AF_OD;
	GPIO_InitStuctrue.GPIO_Pin = GPIO_Pin_7;
	GPIO_InitStuctrue.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStuctrue);

	//IIC结构体成员配置
	I2C_InitStuctrue.I2C_Ack = I2C_Ack_Enable;
	I2C_InitStuctrue.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_10bit;
//	I2C_InitStuctrue.I2C_ClockSpeed = EEPROM_I2C_BAUDRATE;
	I2C_InitStuctrue.I2C_DutyCycle = I2C_DutyCycle_2;
	I2C_InitStuctrue.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStuctrue.I2C_OwnAddress1 = 0x310;
//	I2C_InitStuctrue.I2C_OwnAddress1 = STM32_I2C_OWN_ADDR;
	I2C_Init(I2C1, &I2C_InitStuctrue);
	I2C_Cmd(I2C1, ENABLE);
	
	NVIC_Configuration();

}

void NVIC_Configuration(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = I2C1_EV_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	NVIC_InitStructure.NVIC_IRQChannel = I2C1_ER_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1 ;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;	
	NVIC_Init(&NVIC_InitStructure);

	I2C_ITConfig(I2C1, I2C_IT_EVT | I2C_IT_BUF, ENABLE);
	I2C_ITConfig(I2C1, I2C_IT_ERR, ENABLE); 
}

uint8_t I2C_Buffer_Rx[128] = {0};
uint8_t I2C_Buffer_Tx[128] = {0};
uint32_t RecvLen = 0;
uint8_t RecvFlag = 0;
#if 0
void I2C1_EV_IRQHandler(void)
{
	printf("irq = %02x\r\n",I2C_GetLastEvent(I2C1));
	if(I2C_GetITStatus(I2C1, I2C_IT_AF))
	{
		I2C_ClearITPendingBit(I2C1, I2C_IT_AF);
	}
	switch (I2C_GetLastEvent(I2C1))
	{
		case I2C_EVENT_SLAVE_RECEIVER_ADDRESS_MATCHED:
		break;
		
		case I2C_EVENT_SLAVE_BYTE_RECEIVED:
			I2C_Buffer_Rx[RecvLen++] = I2C_ReceiveData(I2C1);
//			printf("rx = %d, %02x\r\n", RecvLen, I2C_Buffer_Rx[RecvLen]);	
			if (RecvLen >= 15)
			{
				RecvFlag = 1;
				RecvLen = 0;
			}
			
		break;
		
		case I2C_EVENT_SLAVE_STOP_DETECTED:
			I2C_Cmd(I2C1, ENABLE);
		break;
			
	}
}
#endif

#define I2C1_RAM_SIZE           256        // RAM Size in Byte

#define I2C1_MODE_WAITING       0          // Waiting for commands
#define I2C1_MODE_SLAVE_ADR_WR  1          // Received slave address (writing)
#define I2C1_MODE_ADR_BYTE      2          // Received ADR byte
#define I2C1_MODE_DATA_BYTE_WR  3          // Data byte (writing)
#define I2C1_MODE_SLAVE_ADR_RD  4          // Received slave address (to read)
#define I2C1_MODE_DATA_BYTE_RD  5          // Data byte (to read)

uint8_t i2c1_mode = I2C1_MODE_WAITING;
uint8_t i2c1_ram_adr = 0;
uint8_t i2c1_ram[I2C1_RAM_SIZE];

uint8_t Get_I2C1_Ram(uint8_t adr) 
{
    return i2c1_ram[adr];
}

void Set_I2C1_Ram(uint8_t adr, uint8_t val) 
{
    i2c1_ram[adr] = val;
    return;
}

void I2C1_Ram_Init(void) 
{
    uint16_t i;
    for (i = 0; i < 16; i++)
    {
        Set_I2C1_Ram(i, i);
    }
}

void I2C1_ClearFlag(void) 
{
    /* ADDR Flag clear */
    while((I2C1->SR1 & I2C_SR1_ADDR) == I2C_SR1_ADDR) 
    {
        I2C1->SR1;
        I2C1->SR2;
    }

    /* STOPF Flag clear */
    while((I2C1->SR1&I2C_SR1_STOPF) == I2C_SR1_STOPF) 
    {
        I2C1->SR1;
        I2C1->CR1 |= 0x1;
    }
}

void I2C1_EV_IRQHandler(void) 
{
    uint8_t wert;
    uint32_t event;
	uint8_t i;

    /* Reading last event */
    event = I2C_GetLastEvent(I2C1);

    /* Event handle */
    if(event == I2C_EVENT_SLAVE_RECEIVER_ADDRESS_MATCHED) 
    {
        // Master has sent the slave address to send data to the slave
        i2c1_mode = I2C1_MODE_SLAVE_ADR_WR;
    }
    else if(event == I2C_EVENT_SLAVE_BYTE_RECEIVED) 
    {
        // Master has sent a byte to the slave
        wert = I2C_ReceiveData(I2C1);
        // Check address
        if(i2c1_mode == I2C1_MODE_SLAVE_ADR_WR) 
        {
            i2c1_mode = I2C1_MODE_ADR_BYTE;
            // Set current ram address
            i2c1_ram_adr = wert;
        }
        else 
        {
            i2c1_mode = I2C1_MODE_DATA_BYTE_WR;
            // Store data in RAM
            Set_I2C1_Ram(i2c1_ram_adr, wert);
            // Next ram adress
            i2c1_ram_adr++;
			if(i2c1_ram_adr>=255)
			{
				printf("write %d, \r\n", i2c1_ram_adr);
				for(i = 0; i < i2c1_ram_adr; i++)
				{
					printf("%02x ", Get_I2C1_Ram(i));
				}
				printf("\r\n");
			}
        }
    }
    else if(event == I2C_EVENT_SLAVE_TRANSMITTER_ADDRESS_MATCHED) 
    {
        // Master has sent the slave address to read data from the slave
        i2c1_mode = I2C1_MODE_SLAVE_ADR_RD;
        // Read data from RAM
        wert = Get_I2C1_Ram(i2c1_ram_adr);
        // Send data to the master
        I2C_SendData(I2C1, wert);
        // Next ram adress
        i2c1_ram_adr++;
    }
    else if(event == I2C_EVENT_SLAVE_BYTE_TRANSMITTED) 
    {
        // Master wants to read another byte of data from the slave
        i2c1_mode = I2C1_MODE_DATA_BYTE_RD;
        // Read data from RAM
        wert = Get_I2C1_Ram(i2c1_ram_adr);
        // Send data to the master
        I2C_SendData(I2C1, wert);
        // Next ram adress
        i2c1_ram_adr++;
    }
    else if(event == I2C_EVENT_SLAVE_STOP_DETECTED) 
    {
        // Master has STOP sent
        I2C1_ClearFlag();
        i2c1_mode = I2C1_MODE_WAITING;
    }
}

void I2C1_ER_IRQHandler(void)
{
    if(I2C_GetITStatus(I2C1, I2C_IT_AF) != RESET)
    {
        I2C_ClearITPendingBit(I2C1, I2C_IT_AF);
    }
    if(I2C_GetITStatus(I2C1, I2C_IT_BERR) != RESET)
    {
        I2C_ClearITPendingBit(I2C1, I2C_IT_BERR);
    }
}

